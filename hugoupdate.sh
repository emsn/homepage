#!/bin/bash

# Pull latest version
cd /root/blog/emsn/homepage
/usr/bin/git pull
#/usr/bin/git submodule update

# Get KPIs
cd emsn
KPI=`/usr/bin/curl --connect-timeout 500 https://api.emsn.eu/v0/kpis/ 2>/dev/zero`
STATIONS=`/bin/echo ${KPI} | /usr/bin/jq '.stations'`
METEORS=` /bin/echo ${KPI} | /usr/bin/jq '.meteors'`
DATASETS=`/bin/echo ${KPI} | /usr/bin/jq '.datasets'`

/bin/echo "[params.kpi]"                  > kpi.toml
/bin/echo "  enable = true"              >> kpi.toml
/bin/echo "  stations = \"${STATIONS}\"" >> kpi.toml
/bin/echo "  meteors = \"${METEORS}\""   >> kpi.toml
/bin/echo "  datasets = \"${DATASETS}\"" >> kpi.toml

# Build
/bin/rm -rf /root/blog/emsn/homepage/emsn/public/*
/usr/bin/hugo --config config.toml,kpi.toml
